from django import forms
from PIL import Image
from datetime import datetime

from todo_01.models import Task, todoUserProfile

class TaskForm(forms.ModelForm):
    taskText = forms.CharField(max_length=128)
    dateStarted = forms.DateTimeField(widget=forms.HiddenInput(), initial = datetime.now())


    class Meta:
        model = Task
        fields = ('taskText',)


