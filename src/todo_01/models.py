from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Task(models.Model):
    taskText = models.CharField(max_length=128)
    dateStarted = models.DateTimeField(auto_now_add=True)
    dateFinished = models.DateTimeField(null=True, blank=True)
    isFinished = models.BooleanField(default=False)
    author = models.ForeignKey(User)

    def __unicode__(self):
        return self.taskText

class todoUserProfile(models.Model):
    user = models.OneToOneField(User)
    tasksCreated = models.IntegerField(default=0)
    tasksFinished = models.IntegerField(default=0)
    tasksDeleted = models.IntegerField(default=0)
    userProfilePic = models.ImageField(upload_to="upload/", blank=True, null=True)

    def __unicode__(self):
        return self.user.username