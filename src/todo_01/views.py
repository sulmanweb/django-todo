from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.contrib.auth.decorators import login_required
from django.core import serializers
from django.utils import timezone

from todo_01.models import Task, todoUserProfile
from todo_01.forms import TaskForm


@login_required
def index(request):
    context_dict = {}
    user = request.user
    userProf = todoUserProfile.objects.get(user=user)
    context_dict['user'] = user
    if request.method == 'POST':
        form = TaskForm()
        f = form.save(commit=False)
        f.taskText = request.POST['taskText']
        f.author = request.user
        f.dateStarted = timezone.now()
        f.isFinished = False
        f.save()
        userProf.tasksCreated += 1
        userProf.save()
        form = TaskForm
        context_dict['form'] = form
        return render(request, 'todo_01/index.html', context_dict)
    else:
        form = TaskForm
    context_dict['form'] = form
    return render(request, 'todo_01/index.html', context_dict)



@login_required
def indexJS(request):
    context_dict = {}
    user = request.user
    taskList = serializers.serialize('json',Task.objects.filter(author=user))
    context_dict['tasks'] = taskList
    return JsonResponse(context_dict)


@login_required
def modTask(request):
    user = request.user
    userProf = todoUserProfile.objects.get(user=user)
    if request.method == 'POST':
        taskId = request.POST['modTaskId']
        t = Task.objects.get(pk=taskId)
        if request.POST['action'] == 'delete':
            t.delete()
            userProf.tasksDeleted += 1
            userProf.save()
            return HttpResponse("task " + taskId + " deleted")
        elif request.POST['action'] == 'markfinished':
            if t.isFinished:
                t.isFinished = False
                t.dateFinished = None
                userProf.tasksFinished -= 1
            else:
                t.isFinished = True
                t.dateFinished = timezone.now()
                userProf.tasksFinished += 1
            t.save()
            userProf.save()
            return HttpResponse("task " + taskId + " changed")


@login_required
def userProfile(request):
    user = request.user
    userProf = todoUserProfile.objects.get(user=user)
    context_dict = {}
    context_dict['user'] = user
    context_dict['userProfile'] = userProf
    return render(request, 'todo_01/userprofile.html', context_dict)


@login_required
def resetStats(request):
    user = request.user
    userProf = todoUserProfile.objects.get(user=user)
    userProf.tasksDeleted = 0
    userProf.tasksCreated = 0
    userProf.tasksFinished = 0
    userProf.save()
    return redirect('todo_01.views.userProfile')


@login_required
def editProfile(request):
    pass