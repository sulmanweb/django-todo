from django.apps import AppConfig

class todoConfig(AppConfig):
    name = 'todo_01'
    verbose_name = 'ToDo Application'

    def ready(self):
        import todo_01.signals
