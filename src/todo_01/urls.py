from django.conf.urls import patterns, url
from django.conf import settings
from todo_01 import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^indexJS/',views.indexJS, name='indexjs'),
    url(r'^modTask/',views.modTask, name='modTask'),
    url(r'^profile/', views.userProfile, name='userProfile'),
    url(r'^resetstats/', views.resetStats, name='resetStats'),
    url(r'^editprofile/', views.editProfile, name='editProfile'),
    ]
