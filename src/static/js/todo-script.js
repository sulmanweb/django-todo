function getTaskList(callback) {
    $.ajax({
        type: 'GET',
        dataType: "json",
        url: '/todoapp/indexJS/',
    }).done(function (data) {
        var taskList = JSON.parse(data['tasks']);
        var filterOption = $("#filter").find(":selected").val();
        callback(taskList, filterOption);
    });
};

function showTaskList(taskList, filterOption) {
    console.log("FO: " + filterOption);
    $("#taskList").empty();
	if (taskList.length > 0) {
		for (var i = 0; i < taskList.length; i++) {
			var listItem = $("<li class='list-group-item'>").text(taskList[i]["fields"]["taskText"])
			var taskCheckBox = $("<input>")
				.attr("type","checkbox")
				.attr("taskNumber",taskList[i]["pk"]);
			var deleteButton = $("<button>")
				.text("Delete task")
				.addClass("deleteButton pull-right")
				.attr("taskNumber",taskList[i]["pk"]);
			if (taskList[i]["fields"]["isFinished"]) {
				taskCheckBox.prop("checked", true);
				listItem.addClass("finishedSpan");
			} else {
				taskCheckBox.prop("checked", false);
				listItem.removeClass("finishedSpan");
			};
            listItem.append(taskCheckBox).append(deleteButton);
            console.log(taskList[i]["fields"]["isFinished"]);
    		if (filterOption === "all") {
					$("#taskList").append(listItem);
			} else if (filterOption === "active" && taskList[i]["fields"]["isFinished"] === false) {
				$("#taskList").append(listItem);
			} else if (filterOption === "complete" && taskList[i]["fields"]["isFinished"] === true) {
				$("#taskList").append(listItem);
			}
		}
	}
};

function deleteTask(id) {
    var outputData = { 'modTaskId' : id, 'action' : 'delete' };
    $.ajaxSetup({
            headers: { "X-CSRFToken": $("input[name='csrfmiddlewaretoken']").attr("value")}
        });
    $.ajax({
            type: 'POST',
            url:'/todoapp/modTask/',
            data: outputData,
        }).done(getTaskList.bind(this, showTaskList));
};

function markFinished(id) {
    var outputData = { 'modTaskId' : id, 'action' : 'markfinished' };
    $.ajaxSetup({
            headers: { "X-CSRFToken": $("input[name='csrfmiddlewaretoken']").attr("value")}
        });
    $.ajax({
            type: 'POST',
            url:'/todoapp/modTask/',
            data: outputData,
        }).done(getTaskList.bind(this, showTaskList));
};

$(document).ready(function() {

    getTaskList(showTaskList);


    $("#addTaskButton").on("click",function(event) {
        event.preventDefault();
        var formData = $("#taskForm").serializeArray();
        var outputData = {};
        for (var i = 0; i < formData.length; i++) {
            outputData[formData[i]['name']] = formData[i]['value'];
        }

        $.ajaxSetup({
            headers: { "X-CSRFToken": outputData["csrfmiddlewaretoken"]}
        });

        $.ajax({
            type: 'POST',
            url:'/todoapp/',
            data: outputData,
        }).done(getTaskList.bind(this, showTaskList));
        $("#id_taskText").val("");
})

    $("ul").on("click","li .deleteButton",function(event){
		var taskId = $(this).attr("taskNumber");
		deleteTask(taskId);
	})

	$("ul").on("click","li input:checkbox",function(event){
			var taskId = $(this).attr("taskNumber");
			markFinished(taskId);
	});
	$("#filter").on("change", function(event){
		event.preventDefault();
		getTaskList(showTaskList);
	})

});
